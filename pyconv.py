from PIL import Image
import os, sys

def convertPNGtoJPEG(infile, outfile):
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.load()
            bg = Image.new("RGB", im.size, (255,255,255))
            bg.paste(im, mask=im.split()[3])
            bg.save(outfile,'JPEG',quality=80)
        except IOError:
            print("cannot convert", infile)


def main():
    infile = sys.argv[1]#Input file
    outfile = sys.argv[2]#Output file
    convertPNGtoJPEG(infile, outfile)


if __name__ == '__main__':
  main()
